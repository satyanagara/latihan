from odoo import fields,models,api,_
import time


class alatKantor(models.Model):
    _name = 'latihan.alat_kantor'
    _description = 'Desc goes here'
    
    
    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
    
    name = fields.Char(string="Nama",size=64,required=1)
    #html akan menampilkan kolom datam format text editor (sama dgn denga "Text")
    notes = fields.Text(string='Notes')
    partner_id = fields.Many2one('res.partner',string='Used By', required=1)
    categ_id = fields.Many2one('latihan.alat_kantor_categ',string='Kategory')
    reg_date = fields.Date(string="Tgl. Registrasi", default=time.strftime("%Y-%m-%d"))
    
    #field state berhuungan dgn status data. dan bisa ditreatment di view (form view)
    state = fields.Selection([('draft','Draft'),('confirm','Confirmed')], string='Status',default='draft',readonly=1)
    vendor_ids = fields.Many2many('res.partner','latihan_alat_kantor_partner_rel','latihan_id','partner_id',string='Vendors')
    
    #field "active" akan berpengaruh pd muncul atau tidaknya data di tree view
    active = fields.Boolean(string="Active",default=True)
    
class alatKantorCateg(models.Model):
    _name = 'latihan.alat_kantor_categ'
    
    name = fields.Char(string="Category Name",required=1)
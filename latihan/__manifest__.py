# -*- coding: utf-8 -*-
{
    'name': "Latihan",
    'summary': """Custom""",
    'description': """Description goes here""",
    'author': "ERPCentral",
    'website': "http://www.erpcentral.biz",
    'category': 'Custom',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/alat_kantor_view.xml'
        ],
    'demo': [],
}